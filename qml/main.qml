import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.11
import QtQuick.Dialogs.qml 1.0
import QtQuick.Controls 2.15
import Qt3D.Logic 2.0
import QtQuick.XmlListModel 2.0
import Qt.labs.qmlmodels 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ComboBox {
        width: 200
        //model: [ "Banana", "Apple", "Coconut" ]
        ListModel {
               id: cbItems
               ListElement { text: "Banana"; color: "Yellow" }
               ListElement { text: "Apple"; color: "Green" }
               ListElement { text: "Coconut"; color: "Brown" }
           }
}

}
