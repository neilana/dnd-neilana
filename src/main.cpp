#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <Core.h>
#include <DbManager.h>
#include <WorkerThread.h>

#include <iostream>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    using namespace DndNeilana;
    QGuiApplication app(argc, argv);

    //Core core;
    //core.start();

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));

    Controller c;
    c.operate("qwe");

//    QObject::connect(&engine,
//                     &QQmlApplicationEngine::objectCreated,
//                     &app,
//                     [url](QObject *obj, const QUrl &objUrl) {
//                        if (!obj && url == objUrl)
//                            QCoreApplication::exit(-1);
//                      },
//                     Qt::QueuedConnection);

    engine.load(url);

    //QSqlDatabase db = DbManager::getInstance();




    return app.exec();
}
