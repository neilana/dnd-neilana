#pragma once

#include <stdio.h>
#include <sqlite/sqlite3.h>

#include <QSqlDatabase>

namespace DndNeilana
{
class DbManager
{
public:
    static QSqlDatabase& getInstance()
    {
        static DbManager instance; // Guaranteed to be destroyed.
                                      // Instantiated on first use.
        return instance.db_;
    }

private:
    QSqlDatabase db_;
    DbManager()
    {
        db_ = QSqlDatabase::addDatabase("QSQLITE", "dnd-neilana");
        db_.setDatabaseName("../dnd-neilana/data/db/dnd-neilana.db3");
        if (!db_.open())
        {
            //WCNT_LOG("[DB] Error (WordsDB): {}", m_db.lastError().text().toStdString());
        }
    }
};

}
