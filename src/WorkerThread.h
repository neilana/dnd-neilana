#pragma once

//https://doc.qt.io/qt-5/qthread.html

#include <QObject>
#include <QThread>

#include <iostream>

namespace DndNeilana
{

class Worker : public QObject
{
    Q_OBJECT

public slots:
    void doWork(const QString &parameter) {
        QString result;

        std::cout<< "kek\n";
        /* ... here is the expensive or blocking operation ... */
        emit resultReady(result);
    }

signals:
    void resultReady(const QString &result);
};


class Controller : public QObject
{
private:
    Q_OBJECT
    QThread workerThread;

public:
    Controller() {
        Worker *worker = new Worker;
        worker->moveToThread(&workerThread);
        connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);
        connect(this, &Controller::operate, worker, &Worker::doWork);
        connect(worker, &Worker::resultReady, this, &Controller::handleResults);
        workerThread.start();
    }
    ~Controller() {
        workerThread.quit();
        workerThread.wait();
    }

public slots:
    void handleResults(const QString &){};

signals:
    void operate(const QString &);
};
}
